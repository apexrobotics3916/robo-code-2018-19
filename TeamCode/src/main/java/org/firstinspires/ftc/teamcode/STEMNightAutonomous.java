package org.firstinspires.ftc.teamcode;

import com.qualcomm.robotcore.eventloop.opmode.Autonomous;
import com.qualcomm.robotcore.eventloop.opmode.OpMode;
import com.qualcomm.robotcore.util.ElapsedTime;

import org.firstinspires.ftc.robotcore.external.navigation.DistanceUnit;

import java.util.ArrayList;
import java.util.List;

@Autonomous(name = "STEM Night", group = "Club Robot")
public class STEMNightAutonomous extends OpMode {
    Robot bot = new Robot();
    private ElapsedTime runtime = new ElapsedTime();
    private String timeOfCompletion;

    private int state = 0;
    private int orientation;
    private String telemetryMsg;

    @Override
    public void init() {
        bot.init(hardwareMap);
    }

    @Override
    public void start() {
        runtime.reset();
    }

    @Override
    public void stop() {

    }

    @Override
    public void loop() {
        switch (state) {
            // even numbers for performing an action while odd values for state are used for waiting, seen in default case
            case 0:
                new Thread(new Tasker("drive to cube/balls")).start();
                state++;

                break;
            case 2:
                new Thread(new Tasker("scan for cube")).start();
                state++;

                break;
            default:
                break;
        }

        if (telemetryMsg != null) {
            telemetry.addData("*", telemetryMsg);
            telemetry.addData("*", "-----------");
        }

        telemetry.addData("*", "runtime: " + runtime.toString());
        telemetry.update();
    }

    private class Tasker implements Runnable {
        private String task;

        Tasker(String task) {
            this.task = task;
        }

        @Override
        public void run() {
            // first value from split string should be the task to perform
            String[] split = task.split(",");
            boolean found = false;
            switch (split[0]) {
                case "drive":
                    // very basic test encoder drive

                    telemetryMsg = "driving " + Double.parseDouble(split[2]) + "m at " + Double.parseDouble(split[3]) + " power";
                    bot.driveToPosition(split[1], Double.parseDouble(split[2]), Double.parseDouble(split[3]));

                    break;
                case "turn":
                    // very basic automated turning to specific orientation

                    telemetryMsg = "turning " + Double.parseDouble(split[1]) + " rad at " + Double.parseDouble(split[2]) + " power";
                    bot.turn(Double.parseDouble(split[1]), Double.parseDouble(split[2]));

                    break;
                case "drive to cube/balls":
                    found = false;

                    bot.drive("right", 1);

                    while (!found) {
                        int red = bot.CRSensor.red(), green = bot.CRSensor.green(), blue = bot.CRSensor.blue();

                        if ((red > blue && green > blue && (red > green - 10 || green > red - 10) && (red < green + 10 || green < red + 10)) || (red == blue && blue == green) && bot.CRSensor.getDistance(DistanceUnit.METER) < 1.5)
                            found = true;
                    }

                    bot.stopDriving();

                    break;
                case "scan for cube":
                    found = false;

                    bot.drive("left", 1);

                    while (!found) {
                        int red = bot.CRSensor.red(), green = bot.CRSensor.green(), blue = bot.CRSensor.blue();

                        if (red > blue && green > blue && (red > green - 10 || green > red - 10) && (red < green + 10 || green < red + 10)) {
                            bot.stopDriving();

                            bot.driveToPosition("right", .5, 1);
                            bot.driveToPosition("right", -.5, 1);
                            found = true;
                        }
                    }

                    break;
                default:
                    telemetryMsg = split[0] + " is either spelled wrong or hasn't been implemented yet";

                    break;
            }

            state++;
        }
    }
}
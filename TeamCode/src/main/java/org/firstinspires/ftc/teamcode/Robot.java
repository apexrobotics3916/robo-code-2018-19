package org.firstinspires.ftc.teamcode;

import com.qualcomm.robotcore.hardware.*;
import com.qualcomm.hardware.lynx.LynxI2cColorRangeSensor;

import java.util.List;
import java.util.ArrayList;

/**
 * robot class for easier component control across multiple drive mode classes
 * robot functionality handled here for consistency and avoiding bugs and simplicity in driver classes
 * NOTICE: all dimensions are measured in meters (in case not specified). all measurements should be taken in meters or else calculations will absolutely mess up
 * NOTICE: all angles are measured in radians (in case not specified). please don't ever use degrees for angles
 *
 * @author will simpson of apex robotics 3916
 * @since December 2018
 */
public class Robot {
    /*
     * all electronic components for the robot
     */

    // drive motors
    private List<DcMotor> leftDrive = new ArrayList<>();
    private List<DcMotor> rightDrive = new ArrayList<>();

    // arm motor
    private DcMotor grabberArm;

    private Servo grabberLift;
    private Servo grabber;

    // color/distance sensor
    public LynxI2cColorRangeSensor CRSensor;

    // hook motors; one moves lift up, other pulls it down so it can pull the robot up when trying to hang
    private DcMotor hookLift;

    /*
     * any arbitrary constants required for certain calculations
     */

    // METERS; radius of the robot based on the circle traced by its wheels when turning
    private static final double BOT_RADIUS = .535; // not real, actual value tbd
    // METERS; radius of one of the robot's wheels
    private static final double WHEEL_RADIUS = .1; // not real, actual value tbd
    // RADIANS; angle created by the grabber system completely raised against the arm and an abstract horizontal line level with servo
    // NOTICE: this is specific to how the servos are mounted and may differ for other configurations
    public static final double GRABBER_LIFT_UP = 1; // not real, actual value tbd
    // RADIANS; angle created by grabber system equal to abstract horizontal line level with servo
    public static final double GRABBER_LIFT_DOWN = 0; // not real, actual value tbd

    // amount of motor ticks per revolution for 40:1 REV hex motor. other motors may have different TPR
    private static final double TICKS_PER_REV = 2440;

    /**
     * initialize robot components; will allow motors and servos to tighten and not freely move
     *
     * COMPONENT SPECS:
     * motor power range - -1 to 1
     * servo power range - 0 to 1
     * servo angle range (default 180 deg/ pi rad) - 0 to 1
     *
     * @param hw - hardwaremap supplied from drive class
     */
    public void init(HardwareMap hw) {
        leftDrive.add(hw.get(DcMotor.class, "left drive | left"));
        leftDrive.add(hw.get(DcMotor.class, "left drive | right"));
        rightDrive.add(hw.get(DcMotor.class, "right drive | left"));
        rightDrive.add(hw.get(DcMotor.class, "right drive | right"));

        grabberArm = hw.get(DcMotor.class, "grabber arm");

        grabberLift = hw.get(Servo.class, "grabber lift");
        grabber = hw.get(Servo.class, "grabber");

        CRSensor = hw.get(LynxI2cColorRangeSensor.class, "c/d sensor");

        hookLift = hw.get(DcMotor.class, "hook lift");

        // reverse 1 motor per drive side due to corresponding motors being flipped
        leftDrive.get(1).setDirection(DcMotor.Direction.REVERSE);
        rightDrive.get(1).setDirection(DcMotor.Direction.REVERSE);

        // set motors to no power
        for (DcMotor m : leftDrive)
            m.setPower(0);

        for (DcMotor m : rightDrive)
            m.setPower(0);

        grabberArm.setPower(0);

        // set grabber to no power
        grabber.setPosition(0);

        // set servo to completely raised
        grabberLift.setPosition(0);
    }

    public void setDirection(String direction) {
        switch (direction) {
            case "forward":
                leftDrive.get(0).setDirection(DcMotor.Direction.FORWARD);
                leftDrive.get(1).setDirection(DcMotor.Direction.REVERSE);

                rightDrive.get(0).setDirection(DcMotor.Direction.FORWARD);
                rightDrive.get(1).setDirection(DcMotor.Direction.REVERSE);
                break;
            case "backward":
                leftDrive.get(0).setDirection(DcMotor.Direction.REVERSE);
                leftDrive.get(1).setDirection(DcMotor.Direction.FORWARD);

                rightDrive.get(0).setDirection(DcMotor.Direction.REVERSE);
                rightDrive.get(1).setDirection(DcMotor.Direction.FORWARD);
                break;
            case "left":
                leftDrive.get(0).setDirection(DcMotor.Direction.REVERSE);
                leftDrive.get(1).setDirection(DcMotor.Direction.FORWARD);

                rightDrive.get(0).setDirection(DcMotor.Direction.FORWARD);
                rightDrive.get(1).setDirection(DcMotor.Direction.REVERSE);
                break;
            case "right":
                leftDrive.get(0).setDirection(DcMotor.Direction.FORWARD);
                leftDrive.get(1).setDirection(DcMotor.Direction.REVERSE);

                rightDrive.get(0).setDirection(DcMotor.Direction.REVERSE);
                rightDrive.get(1).setDirection(DcMotor.Direction.FORWARD);
                break;
        }
    }

    public void flip(String drive) {
        List<DcMotor> motors = new ArrayList<>();

        if (drive.equals("left") || drive.equals("all")) {
            motors.addAll(leftDrive);
        }

        if (drive.equals("right") || drive.equals("all")) {
            motors.addAll(rightDrive);
        }

        for (DcMotor m : motors) {
            if (m.getDirection() == DcMotor.Direction.FORWARD)
                m.setDirection(DcMotor.Direction.REVERSE);
            else
                m.setDirection(DcMotor.Direction.FORWARD);
        }
    }

    /**
     * simple motor-power-based linear drive method
     * @param drive - specify left or right side of motor drive
     * @param power - -1 to 1; power to supply to each motor; motors should all have same power for smooth turning
     */
    public void drive(String drive, double power) {
        if (drive.equals("left") || drive.equals("all")) {
            for (DcMotor m : leftDrive)
                m.setPower(power);
        }

        if (drive.equals("right") || drive.equals("all")) {
            for (DcMotor m : rightDrive)
                m.setPower(power);
        }
    }

    /**
     * stops motor-power-based movement by setting motor power to 0
     */
    public void stopDriving() {
        drive("all", 0);
    }

    /**
     * simple motor-power-based turn method
     * @param power - -1 to 1; power to supply to each motor; motors should all have same power for smooth turning
     */
    public void turn(double power) {
        // set reversed motors to forward
        leftDrive.get(1).setDirection(DcMotor.Direction.FORWARD);
        rightDrive.get(1).setDirection(DcMotor.Direction.FORWARD);

        for (DcMotor m : leftDrive)
            m.setPower(power);

        for (DcMotor m : rightDrive)
            m.setPower(power);

        // re-reverse proper motors
        leftDrive.get(1).setDirection(DcMotor.Direction.REVERSE);
        rightDrive.get(1).setDirection(DcMotor.Direction.REVERSE);
    }

    /**
     * encoder-based turn method for turning a specific amount
     * @param theta - RADIANS; angle of turn relative to the circle drawn by the rotating robot measured in the robot's center
     * @param power - -1 to 1; power to supply to each motor; motors should all have same power for smooth turning
     */
    public void turn(double theta, double power) {
        // all motors must be in same direction
        if (theta > 0) {
            leftDrive.get(1).setDirection(DcMotor.Direction.FORWARD);
            rightDrive.get(1).setDirection(DcMotor.Direction.FORWARD);
        } else {
            leftDrive.get(0).setDirection(DcMotor.Direction.REVERSE);
            rightDrive.get(0).setDirection(DcMotor.Direction.REVERSE);
        }

        /*
         * arc length needed in order to know the distance for each encoder to reach for turning
         * arc length = theta * radius
         * pass arc length into encoderDrive()
         */
        if (theta > 0)
            encoderDrive("all", theta * BOT_RADIUS, power);
        else
            encoderDrive("all", -theta * BOT_RADIUS, power);

        // re-reverse proper motors
        if (theta > 0) {
            leftDrive.get(1).setDirection(DcMotor.Direction.REVERSE);
            rightDrive.get(1).setDirection(DcMotor.Direction.REVERSE);
        } else {
            leftDrive.get(0).setDirection(DcMotor.Direction.FORWARD);
            rightDrive.get(0).setDirection(DcMotor.Direction.FORWARD);
        }
    }

    /**
     * encoder-based linear drive method for moving a specific distance
     * only calls encoderDrive() but should still exist for simpler implementation and avoiding repeating code in encoder turn()
     * @param drive - specify left or right side of motor drive
     * @param distance - METERS; projected distance for robot to move
     * @param power - -1 to 1; power to supply to each motor; motors should all have same power for smooth turning
     */
    public void driveToPosition(String drive, double distance, double power) {
        if (distance > 0)
            encoderDrive(drive, distance, power);
        else {
            // reverse current direction of both motors in order to move
            // in opposite direction
            if (drive.equals("left")) {
                leftDrive.get(1).setDirection(DcMotor.Direction.FORWARD);
                leftDrive.get(0).setDirection(DcMotor.Direction.REVERSE);
            } else {
                rightDrive.get(1).setDirection(DcMotor.Direction.FORWARD);
                rightDrive.get(0).setDirection(DcMotor.Direction.REVERSE);
            }

            encoderDrive(drive, -distance, power);

            // re-reverse current direction of both motors
            if (drive.equals("left")) {
                leftDrive.get(1).setDirection(DcMotor.Direction.REVERSE);
                leftDrive.get(0).setDirection(DcMotor.Direction.FORWARD);
            } else {
                rightDrive.get(1).setDirection(DcMotor.Direction.REVERSE);
                rightDrive.get(0).setDirection(DcMotor.Direction.FORWARD);
            }
        }
    }

    /**
     * actual implementation of encoder-based drive. not to be accessed outside the Robot class for simplicity of implementation of other encoder methods in the drive classes
     * use of separate classes for designated functions removes repeated code :)
     * @param drive - specify left or right side of motor drive
     * @param distance - METERS; projected distance for robot to move
     * @param power - -1 to 1; power to supply to each motor; motors should all have same power for smooth turning
     */
    private void encoderDrive(String drive, double distance, double power) {
        // list of motors in use to avoid repeated code and lots of if statements
        List<DcMotor> motorsInUse = new ArrayList<>();

        if (drive.equals("left") || drive.equals("all"))
            motorsInUse.addAll(leftDrive);

        if (drive.equals("right") || drive.equals("all"))
            motorsInUse.addAll(rightDrive);

        // stop and reset all encoders
        for (DcMotor m : leftDrive)
            m.setMode(DcMotor.RunMode.STOP_AND_RESET_ENCODER);

        for (DcMotor m : rightDrive)
            m.setMode(DcMotor.RunMode.STOP_AND_RESET_ENCODER);

        // find how many turns to drive distance; (distance / circumference) * ticks per revolution
        int target = (int)((distance / (2 * Math.PI * WHEEL_RADIUS)) * TICKS_PER_REV);

        // apply target to each motor being used to drive
        for (DcMotor m : motorsInUse) {
            m.setTargetPosition(target);
            m.setMode(DcMotor.RunMode.RUN_TO_POSITION);
        }

        // set power to motors
        drive(drive, power);

        // wait until all motors are done running
        while (motorsAreBusy(motorsInUse));

        stopDriving();
    }

    /**
     * determines if any motors supplied are busy (only return false when !isBusy() for all)
     * @param motors - list of motors to test
     * @return - true if >= 1 are busy, false if 0 are busy
     */
    private boolean motorsAreBusy(List<DcMotor> motors) {
        int finished = 0;

        for (DcMotor m : motors) {
            if (!m.isBusy())
                finished++;

            if (finished == motors.size())
                return false;
        }

        return true;
    }

    /**
     * sets position of grabberLift based on supplied angle
     * @param theta - RADIANS; 0 to pi; supplied angle to scale to servo position
     */
    public void useGrabberLift(double theta) {
        grabberLift.setPosition(theta / Math.PI);
    }

    /**
     * controls grabber arm to lift or lower
     * @param power - -1 to 1; power to supply to motor
     */
    public void useGrabberArm(double power) {
        grabberArm.setPower(power);
    }

    /**
     * controls the hook mechanism that locks onto the lander's hook to suspend the robot
     * @param power - power to supply to motor
     */
    public void useHookLift(double power) {
        hookLift.setPower(power);
    }

    /**
     * controls the claw to grab objects
     * @param theta - RADIANS; angle to give to servo
     */
    public void useGrabber(double theta) {
        grabber.setPosition(theta / Math.PI);
    }

    /**
     * start a thread to pause an action but not pausing all other concurrent actions
     */
    private class Wait implements Runnable {
        private String component;
        private long duration;

        Wait(String component, long duration) {
            this.component = component;
            this.duration = duration;
        }

        @Override
        public void run() {
            try {
                // do something if we need to use this

                Thread.sleep(duration);
            } catch (InterruptedException e) {
                return;
            }
        }
    }
}
// by club robot :)
// Revision 12/6/2018

package org.firstinspires.ftc.teamcode;

import com.qualcomm.robotcore.eventloop.opmode.OpMode;
import com.qualcomm.robotcore.eventloop.opmode.TeleOp;
import com.qualcomm.robotcore.hardware.DcMotor;
import com.qualcomm.robotcore.util.ElapsedTime;

import java.util.List;
import java.util.ArrayList;

@TeleOp(name="The Best TeleOp", group="Club Robot")
//@Disabled
public class TeleOpCode extends OpMode {

    private Robot bot = new Robot();

    @Override
    public void init() {
        bot.init(hardwareMap);
    }

    @Override
    public void loop() {
        final double STICK_DEAD_ZONE = 0.05;
        double MOTOR_POWER = 0.5;   

        if (gamepad1.left_stick_y < -STICK_DEAD_ZONE || gamepad1.left_stick_y > STICK_DEAD_ZONE) {
            bot.drive("left", gamepad1.left_stick_y * MOTOR_POWER);
        } else if (gamepad1.left_stick_x < -STICK_DEAD_ZONE || gamepad1.left_stick_x > STICK_DEAD_ZONE) {
            bot.turn(gamepad1.left_stick_x);
        } else {
            bot.drive("left", 0);
        }

        if (gamepad1.right_stick_y < -STICK_DEAD_ZONE || gamepad1.right_stick_y > STICK_DEAD_ZONE) {
            bot.drive("right", gamepad1.right_stick_y);
        } else if (gamepad1.right_stick_x < -STICK_DEAD_ZONE || gamepad1.right_stick_x > STICK_DEAD_ZONE) {
            bot.turn(gamepad1.right_stick_x);
        } else {
            bot.drive("right", 0);
        }

        if (gamepad2.left_stick_y > STICK_DEAD_ZONE || gamepad2.left_stick_y < -STICK_DEAD_ZONE)
            bot.useGrabberArm(-gamepad2.left_stick_y * MOTOR_POWER);
        else
            bot.useGrabberArm(0);

        if (gamepad2.right_stick_y > STICK_DEAD_ZONE || gamepad2.right_stick_y < -STICK_DEAD_ZONE)
            bot.useHookLift(gamepad2.right_stick_y);
        else
            bot.useHookLift(0);

        if (gamepad2.right_trigger > 0)
            bot.useGrabber(-gamepad2.right_trigger * MOTOR_POWER);
        else if (gamepad2.left_trigger > 0)
            bot.useGrabber(gamepad2.left_trigger);
        else
            bot.useGrabber(0);
    }
}

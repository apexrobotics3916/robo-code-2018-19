package org.firstinspires.ftc.teamcode;

import com.qualcomm.robotcore.eventloop.opmode.Autonomous;
import com.qualcomm.robotcore.eventloop.opmode.OpMode;
import com.qualcomm.robotcore.util.ElapsedTime;

import org.firstinspires.ftc.robotcore.external.navigation.DistanceUnit;

import java.util.Arrays;
import java.util.ArrayList;
import java.util.List;

@Autonomous(name = "Big Autonomous Energy", group = "Club Robot")
public class AutoCode extends OpMode {
    private Robot bot = new Robot();
    private ElapsedTime runtime = new ElapsedTime();
    private String timeOfCompletion;

    private int state = 0;
    private String telemetryMsg;

    @Override
    public void init() {
        bot.init(hardwareMap);
    }

    @Override
    public void start() {
        runtime.reset();
    }

    @Override
    public void stop() {
        bot.stopDriving();
    }

    @Override
    public void loop() {
        switch (state) {
            // even numbers for performing an action while odd values for state are used for waiting, seen in default case
           /*case 0:
               // put the bot on the ground
               new Thread(new Tasker("lower from hook")).start();
               state++;

               break;*/
            case 0:
                // turn away so right drive faces proper way
                new Thread(new Tasker("turn," + (Math.PI / 4) + ",1")).start();
                state++;

                break;
           /*case 4:
               // lower hook arm
               new Thread(new Tasker("retract hook arm")).start();
               state++;

               break;*/
            case 2:
                // drive out of the lander box area
                new Thread(new Tasker("drive,right,.8,1")).start();
                state++;

                break;
            case 4:
                // drive 'sideways' (opposide drive) to scan for gold mineral
                new Thread(new Tasker("knock off gold mineral")).start();
                state++;

                break;
            case 6:
                // drive to aligned center but toward nav target
                // ** i don't know what to put here specifically because i have to find how to measure distance traveled bc of knocking mineral
                state++;

                break;
            case 8:
                // drive until .1 meters away from nav target
                new Thread(new Tasker("drive to nav target")).start();
                state++;

                break;
            case 10:
                // scan picture to determine where robot needs to move in order to reach marker zone
                new Thread(new Tasker("scan nav target")).start();
                state++;

                break;
            case 12:
                // turn so one drive side is perpendicular to marker zone, other perpendicular to field edge
                new Thread(new Tasker("turn," + -Math.PI / 4 + ",1")).start();
                state++;

                break;
            case 14:
                // move until reaching marker zone
                new Thread(new Tasker("drive to marker zone")).start();
                state++;

                break;
            case 16:
                // place marker
                new Thread(new Tasker("place marker")).start();
                state++;

                break;
            case 18:
                // drive backwards until parked on crater
                new Thread(new Tasker("drive to crater")).start();
                state++;

                break;
            case 20:
                // autonomous is done as robot has parked
                telemetryMsg = "autonomous completed in " + timeOfCompletion + " seconds";

                break;
            default:
                break;
        }

        telemetry.addData("runtime", runtime.toString() + " seconds");

        if (telemetryMsg != null) {
            telemetry.addData("*", "-----------");
            telemetry.addData("*", telemetryMsg);
        }

        telemetry.update();
    }

    private class Tasker implements Runnable {
        private String task;
        private String orientation;

        Tasker(String task) {
            this.task = task;
        }

        @Override
        public void run() {
            // first value from split string should be the task to perform
            String[] split = task.split(",");

            switch (split[0]) {
                case "drive":
                    // very basic test encoder drive

                    telemetryMsg = "driving " + Double.parseDouble(split[2]) + "m at " + Double.parseDouble(split[3]) + " power";
                    bot.driveToPosition(split[1], Double.parseDouble(split[2]), Double.parseDouble(split[3]));
                    telemetryMsg = "finished driving " + Double.parseDouble(split[2]) + "m";

                    break;
                case "turn":
                    // very basic automated turning to specific orientation

                    telemetryMsg = "turning " + Double.parseDouble(split[1]) + " rad at " + Double.parseDouble(split[2]) + " power";
                    bot.turn(Double.parseDouble(split[1]), Double.parseDouble(split[2]));
                    telemetryMsg = "finished turning " + Double.parseDouble(split[1]);

                    break;
                case "lower from hook":
                    telemetryMsg = "lowering bot from hook";

                    // have to just rotate up completely so that the robot is then on the ground
                    bot.useHookLift(.2);

                    try {
                        Thread.sleep(3000);
                    } catch (InterruptedException e) {
                        telemetry.addData("error", e.getMessage());
                    }

                    telemetryMsg = "finished lowering";
                    bot.useHookLift(0);

                    break;
                case "retract hook arm":
                    telemetryMsg = "retracting hook";

                    // putting hook arm back in its fully lowered position so we don't run into
                    // any problems w it not being retracted later
                    bot.useHookLift(-1);

                    try {
                        Thread.sleep(3000);
                    } catch (InterruptedException e) {
                        telemetry.addData("error", e.getMessage());
                    }

                    telemetryMsg = "hook retracted";
                    bot.useHookLift(0);

                    break;
                case "knock off gold mineral":
                    telemetryMsg = "preparing to scan for gold mineral";

                    long beginTime; // measuring time used to scan all objects
                    long endTime;

                    bot.driveToPosition("left", .5, 1);

                    beginTime = (long)runtime.time();

                    telemetryMsg = "scanning gold mineral";
                    bot.drive("left", -1);

                    while (true) {
                        // scanned a yellow object
                        if (bot.CRSensor.red() > bot.CRSensor.blue() && bot.CRSensor.green() > bot.CRSensor.blue()) {
                            telemetryMsg = "found gold mineral";

                            endTime = (long)runtime.time();
                            bot.driveToPosition("left", -.1, 1);

                            break;
                        }
                    }

                    telemetryMsg = "knocking off mineral";

                    // drive forward a little bit so claw pushes mineral off the pad
                    bot.driveToPosition("right", .2, 1);
                    // drive backward amount driven forward
                    bot.driveToPosition("right", -.2, 1);

                    // drive back to starting position
                    bot.driveToPosition("left", .1, 1);
                    bot.drive("left", 1);

                    try {
                        Thread.sleep(endTime - beginTime);
                    } catch (InterruptedException e) {
                        telemetry.addData("error", e.getMessage());
                    }

                    bot.driveToPosition("left", -.5, 1);

                    break;
                case "drive to nav target":
                    telemetryMsg = "driving to nav target";

                    // drive until robot senses a nav target .1 meters away

                    bot.drive("right", 1);

                    while (true) {
                        if (bot.CRSensor.getDistance(DistanceUnit.METER) < .3) {
                            telemetryMsg = "found nav target";
                            break;
                        }
                    }

                    bot.stopDriving();

                    break;
                case "scan nav target":
                    telemetryMsg = "scanning nav target";

                    // scan target to determine orientation to marker zone
                    bot.drive("left", 1);

                    //while (true) {
                    if (bot.CRSensor.red() > bot.CRSensor.green() || bot.CRSensor.blue() > bot.CRSensor.green()) {
                        telemetryMsg = "orientation BACK WALL";
                        orientation = "right";
                        //break;
                    } else if (colorsAreSimilar(Arrays.asList(bot.CRSensor.red(), bot.CRSensor.green(), bot.CRSensor.blue()))) {
                        telemetryMsg = "orientation RED ALLIANCE WALL";
                        orientation = "left";
                        //break;
                    } else if (bot.CRSensor.red() > bot.CRSensor.green() && bot.CRSensor.red() > bot.CRSensor.blue()) {
                        telemetryMsg = "orientation FRONT WALL";
                        orientation = "right";
                        //break;
                    } else {
                        telemetryMsg = "orientation BLUE ALLIANCE WALL";
                        orientation = "left";
                        //break;
                    }
                    //}

                    bot.stopDriving();

                    break;
                case "drive to marker zone":
                    telemetryMsg = "driving to marker zone";
                    // drive until tape marking team marker zone is found, then drive a little bit farther inward
                    if (orientation.equals("left"))
                        bot.turn(Math.PI / 4, 1);
                    else
                        bot.turn(-Math.PI / 4, 1);

                    bot.drive("right", 1);

                    while (true) {
                        if ((bot.CRSensor.red() > bot.CRSensor.green() && bot.CRSensor.red() > bot.CRSensor.blue()) ||
                                (bot.CRSensor.blue() > bot.CRSensor.red() && bot.CRSensor.blue() > bot.CRSensor.green())) {
                            telemetryMsg = "found marker zone";
                            break;
                        }
                    }

                    bot.driveToPosition("right", .5, 1);

                    break;
                case "place marker":
                    telemetryMsg = "placing marker";

                    // open claw to drop team marker
                    bot.useGrabber(0);

                    telemetryMsg = "marker placed";

                    break;
                case "drive to crater":
                    telemetryMsg = "driving to crater";

                    // driving until parked on crater
                    bot.driveToPosition("right", -3.5, 1);

                    telemetryMsg = "parked on crater";
                    timeOfCompletion = runtime.toString();

                    break;
                default:
                    telemetryMsg = split[0] + " is either spelled wrong or hasn't been implemented yet";

                    break;
            }

            state++;
        }

        private boolean colorsAreSimilar(List<Integer> colors) {
            for (int i = 0; i < colors.size() - 1; i++) {
                if (colors.get(i) > colors.get(i + 1) + 10 || colors.get(i) < colors.get(i + 1) - 10)
                    return false;
            }

            return true;
        }
    }
}
